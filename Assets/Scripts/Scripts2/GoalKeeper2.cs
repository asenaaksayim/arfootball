﻿using UnityEngine;
using System.Collections;

public class GoalKeeper2 : MonoBehaviour
{
    public float speed = 20f;
    public float horizontalDirection = -1; //direction of movement

    public GameObject kidRight;// right corner of the goal post
    public GameObject kidLeft;// left corner of the goal post
  
 
    int flag = 0;

 
    // Update is called once per frame
    void Update()
    {

        if (transform.position.y <= kidRight.transform.position.y && flag == 0 && maingame2.timer > 0)
        {
            //move goalkeeper  from left corner to right corner
            transform.Translate(new Vector3(0, 0, -speed * horizontalDirection * 1.5f));

            if (transform.position.y >= kidRight.transform.position.y )
            {
                flag = 1;
            }
        }

         else if (transform.position.y>= kidLeft.transform.position.y && flag == 1 && maingame2.timer>0)
        {
            //move goalkeeper from right corner to left corner
            transform.Translate(new Vector3(0, 0, speed * horizontalDirection * 1.5f));
            if (transform.position.y <= kidLeft.transform.position.y)
            {
                flag = 0;
            }
        }

    }

}
