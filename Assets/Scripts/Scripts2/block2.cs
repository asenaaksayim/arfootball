﻿using UnityEngine;
using System.Collections;

public class block2 : MonoBehaviour
{
    float timer = 0;
    public int flagTime = 0;


    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "keeper")
        {
            Debug.Log("touch kid");
            flagTime = 1;
            timer = 0;
        }
  

    }
    void Update()
    {
        if (flagTime == 0)
        {
            timer = 0;

        }
        if (flagTime == 1)
        {
            timer += Time.deltaTime;
        }

        if (timer >= 0.5f)
        {

            maingame2.ball.rigidbody.isKinematic = true;
            maingame2.ball.transform.position = maingame2.firingPoint.transform.position;
            flagTime = 0;
            maingame2.shoot = false;
        }

    }
}
