﻿using UnityEngine;
using System.Collections;
using System.IO;

public class Goal2 : MonoBehaviour
{


    public GameObject GoalGui;
    float timerGui = 0;
    bool flag_goal = false;
    public static int cnt = 0;
    public static int highScore;

    //file R/W variables
    public static string fileName = "";
    StreamWriter fileWriter;
    public string[] textread;

    void Start()
    {
        GoalGui.renderer.enabled = false;
        cnt = 0;
    }

    void Update()
    {
        #if UNITY_ANDROID
        fileName = Application.persistentDataPath + "/" + "/settingsAr.txt";
        #else
	    fileName = Application.dataPath + "/" + "/settingsAr.txt";
        #endif

    

        if (!File.Exists(fileName))
        {
            fileWriter = File.CreateText(fileName);
            fileWriter.WriteLine(highScore);
            fileWriter.Close();
        }
        else
        {
          if (maingame2.timer <= 0 || maingame2.esc ==true )
            {
                int intgr;
                textread = File.ReadAllLines(fileName);

                foreach (string odczyt in textread)
                {
                    int.TryParse(odczyt, out intgr);

                    if (intgr < highScore)
                    {
                        intgr = highScore;

                    }
                    File.WriteAllText(fileName, intgr.ToString());
                }
            }
        }
        
        if (flag_goal == true)
        {
            timerGui += Time.deltaTime;
        }

        if (timerGui >= 1.5f)
        {
            GoalGui.renderer.enabled = false;
            timerGui = 0;
            flag_goal = false;
        }
        Debug.Log(cnt);

        
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "ball")
        {
            cnt++;         
            highScore = cnt;
            GoalGui.renderer.enabled = true;
            flag_goal = true;

            maingame2.ball.rigidbody.isKinematic = true;
            maingame2.ball.transform.position = maingame2.firingPoint.transform.position;
            maingame2.shoot = false;


        }
    }
}
