﻿using UnityEngine;
using System.Collections;

public class maingame2 : MonoBehaviour
{
    public Animator anim; //to control ArromAnimation

    //static gameobjects are used by other classes
    public static GameObject ball;
    public static GameObject firingPoint;
    public static GameObject ballClone;

    //Variables that hold time
    public static float timer = 40f; //Game count down
    float tempGameTime = 0; // When the game is paused, tempGameTime hold game time until game resumes.
    public static float gameTime = 0f; //hold in-game time
    float Dividetime = 0f;// time variable for calculating direction of the shot
    float AnimTime = 1.33f; // Arrow Animation time length
    
    //bool
    public static bool shoot = false;
    public static bool esc = false;

    // Text Variables
    string textTime;
    public TextMesh TimeText;
    string text;
    public TextMesh Scoretext;

    //Gameobjects
    public GameObject Retry;
    public GameObject goalPost;
    public GameObject ball_bonus;
    

    void Awake()
    {
        Screen.orientation = ScreenOrientation.LandscapeLeft;
        Screen.SetResolution(1920, 1080, true);
    }
    // Use this for initialization
    void Start()
    {
        ball = GameObject.Find("top");
        firingPoint = GameObject.Find("firingPoint");
      
        ball.transform.position = firingPoint.transform.position;
        Time.timeScale = 1;
        timer = 40;
        gameTime = 0;
        esc = false;
        Retry.active = false;
        shoot = false;

        InvokeRepeating("Spawn", 5.0f, 5.0f); //bonus ball spawn
    }

    // Update is called once per frame
    void Update()
    {      
        gameTime += Time.deltaTime;
        Dividetime = (gameTime % AnimTime); // calculate divide time on bar animation by relating between game time and animation time 

        if (timer > 0)
        {
            timer -= Time.deltaTime;
            textTime = timer.ToString("00");
            TimeText.text = textTime;
        }

        if (timer <= 0)
        {
            Retry.active = true;
            Time.timeScale = 0f;
        }

        if (Input.GetMouseButtonDown(0) && shoot == false)
        {          
            tempGameTime = gameTime;
            anim.speed = 0f;
            ball.rigidbody.isKinematic = false;
       
            //very right
            if ((Dividetime >= 0 && Dividetime <= 0.10f))
                 goal(-0.9f);
            if (Dividetime >= 1.23f && Dividetime <= 1.33f)
                 goal(-0.9f);
            //right
            if ((Dividetime > 0.10f && Dividetime < 0.30f))
                 goal(-0.5f);
            if (Dividetime > 1.03f && Dividetime < 1.23f)
                goal(-0.5f);
            //mid
            if ((Dividetime >= 0.30f && Dividetime < 0.40f))
                goal(0);
            if (Dividetime > 0.93f && Dividetime <= 1.03f)
                 goal(0);
            //left
             if ((Dividetime >= 0.40f && Dividetime < 0.60f))
                goal(0.5f);
             if (Dividetime > 0.73f && Dividetime <= 0.93f)
                 goal(0.5f);
            //very left
              if (Dividetime >= 0.60f && Dividetime <= 0.73f)
                 goal(0.8f);

        }

            text = Goal2.cnt.ToString();
            Scoretext.text = text;

            if (firingPoint.transform.position == ball.transform.position)
            {
                anim.speed = 1.0f;
            }

            else
            {              
                anim.speed = 0.0f;
                gameTime = tempGameTime;      
            }

            if (Input.GetKeyDown(KeyCode.Escape))
            {
                esc = true;
                Application.LoadLevel(0);            
            }
    }

    void goal(float a)
    {
        shoot = true;
  
        ball.rigidbody.AddTorque(0, -300, 0);

        ball.rigidbody.AddForce(250, 90 * (a), 0);

    }

    void Spawn()
    {
        Vector3[] spawnpos = {     new Vector3(298f,184f, 126f),
                                   new Vector3(298f,143f, 126f),
                                   new Vector3(298f,121f, 126f),
                                   new Vector3(298f,103f, 126f)};   

        int randomSpawnIndex = UnityEngine.Random.Range(0, 3);
        ballClone = Instantiate(ball_bonus, spawnpos[randomSpawnIndex], Quaternion.identity) as GameObject; //create clone object
   
        if(ballClone!=null)
        Destroy(ballClone,3);
    }


}
