﻿using UnityEngine;
using System.Collections;

public class miss : MonoBehaviour {

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "ball")
        {
            maingame2.ball.rigidbody.isKinematic = true;
            maingame2.ball.transform.position = maingame2.firingPoint.transform.position;
            maingame2.shoot = false;
        }
    }
}
