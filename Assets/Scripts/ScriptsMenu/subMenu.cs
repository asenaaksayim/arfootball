﻿using UnityEngine;
using System.Collections;

public class subMenu : MonoBehaviour {
    public static GameObject submenu;
    bool flag = false;
   
	// Use this for initialization
	void Start () {
        submenu = GameObject.FindGameObjectWithTag("sub");
        submenu.active = false;
	}
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit(); 
    }
 
    void OnMouseDown()
    {
        submenu.active = true;
        if (flag == true)
            submenu.active = false;
    }
    void OnMouseUp()
    {
        if (flag == false)
            flag = true;
        if (submenu.active == false)
            flag = false;
    }
}
