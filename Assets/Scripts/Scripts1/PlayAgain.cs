﻿using UnityEngine;
using System.Collections;

public class PlayAgain : MonoBehaviour {

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.LoadLevel(0);
    }
    void OnMouseDown()
    {
        Application.LoadLevel(1);
    }
}
