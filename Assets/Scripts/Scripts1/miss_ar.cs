﻿using UnityEngine;
using System.Collections;

public class miss_ar : MonoBehaviour {

    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "ball")
        {
            main_game.ball.rigidbody.isKinematic = true;
            main_game.ball.transform.position = main_game.firingPoint.transform.position;
            main_game.shoot = false;
        }
    }
}
