﻿using UnityEngine;
using System.Collections;
using System.IO;

public class HighScore : MonoBehaviour {
    public static int high;
    public TextMesh Timetext;
    string[] texti;
    string text;
  //  StreamReader fileReader = null;
    public static string fileName = "";
	// Use this for initialization
	void Start () {
       
        #if UNITY_ANDROID
        fileName = Application.persistentDataPath + "/" + "/settingsAr.txt";
        #else
	        fileName = Application.dataPath + "/" + "/settingsAr.txt";
        #endif

        if (File.Exists(fileName))
        {
            texti = File.ReadAllLines(fileName);
            text = texti[0];
            Timetext.text = text;
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
