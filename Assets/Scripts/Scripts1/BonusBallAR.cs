﻿using UnityEngine;
using System.Collections;

public class BonusBallAR : MonoBehaviour {

    GameObject bonus;
    bool touch = false;

    void Start()
    {
        bonus = GameObject.Find("bonus");
    }

    void Update()
    {
        if (touch == false)
            bonus.renderer.enabled = false;


    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "ball")
        {

            main_game.timer += 3;
            bonus.renderer.enabled = true;
            touch = true;
            Destroy(main_game.ballClone);
        }

    }
}
