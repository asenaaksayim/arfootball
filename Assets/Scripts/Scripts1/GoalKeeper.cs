﻿using UnityEngine;
using System.Collections;

public class GoalKeeper : MonoBehaviour {
    public float speed = 20f;
    public float horizontalDirection = -1; //direction of movement

   public GameObject kidRight;// right corner of the goal post
    public GameObject kidLeft;// left corner of the goal post
   
    int flag = 0;

	
	// Update is called once per frame
    void Update()
    {

            if (transform.position.z <= kidRight.transform.position.z && flag == 0 && main_game.timer>0) 
            {
                //move goalkeeper  from left corner to right corner

                transform.Translate(new Vector3(0, 0, -speed * horizontalDirection * 1.5f));
                if (transform.position.z >= kidRight.transform.position.z)
                {   
                   flag = 1;             
                }
            }

            else if (transform.position.z >= kidLeft.transform.position.z && flag == 1 && main_game.timer > 0) 
            {
                //move goalkeeper from right corner to left corner
                
                transform.Translate(new Vector3(0, 0, speed * horizontalDirection * 1.5f));
                if (transform.position.z <= kidLeft.transform.position.z)
                {
                    flag = 0;                   
                }
            }

    }
   
}
